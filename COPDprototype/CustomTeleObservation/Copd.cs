﻿using System;

namespace CustomTeleObservation
{
    public class Copd 
    {
        public int AvgHr { get; set; }
        public int MinSpo { get; set; }
        public int MaxSpo { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

		public Copd() { }

		public Copd(int avgHr, int minSpo, int maxSpo, DateTime startTime, DateTime endTime)
		{
		    AvgHr = avgHr;
			MinSpo = minSpo;
            MaxSpo = maxSpo;
            StartTime = startTime;
            EndTime = endTime;
		}

		public override string ToString()
		{
            return AvgHr.ToString() + "," + MinSpo.ToString() + "," + MaxSpo.ToString() + "," + StartTime.Ticks.ToString() + "," + EndTime.Ticks.ToString(); 
		}
    }
}
