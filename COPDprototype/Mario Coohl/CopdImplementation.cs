﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using MarioKol.ViewModels;
using MarioKol.Views;
using CAALHP.Events.Types;

namespace MarioKol
{
    public class CopdImplementation : IAppCAALHPContract
    {   

        #region Properties and members

        private readonly GameWindow _mainWindow;
        private readonly MainWindowViewModel _vm;

        // event stuff
        public event EventHandler Event;
        public EventArgs e = null;
        public delegate void EventHandler(UserProfile userProfile, EventArgs e);

        // CAALHP Stuff
        private IAppHostCAALHPContract _host { get; set; }
        private Thread _thread;
        private int _processId;
        private volatile Application _app;
        private const string AppName = "Mario Cohl";

        #endregion

        public CopdImplementation(MainWindowViewModel vm)
        {
            _vm = vm;
        }

        #region CAALHP Specifics
        
        public string GetName()
        {
            return AppName;
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);

            HandleEvent(obj);
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            // _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            

            // Subscribe to ShowAppEvents
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IsUserLoggedInResponsEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserListResponseEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(OpenMarioKolEvent)), _processId);

        }

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        public void Show()
        {
            _vm.Show();
        }
        
        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

        #region Eventrelated

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                Show();
            }
        }


        private void HandleEvent(IsUserLoggedInResponsEvent o)
        {
            Event(o.User, e);
        }

        private void HandleEvent(OpenMarioKolEvent e)
        {
            MessageBox.Show("OpenMarioKolEvent modtaget.");
            Show();
        }

        public void SendIsUserLoggedInRequestEvent()
        {
            Task.Run(() =>
            {
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, new IsUserLoggedInRequestEvent(), "Mario Cohl");
                _host.Host.ReportEvent(serializedCommand);
            });
        }

        public void SendUserListRequestEvent()
        {
            Task.Run(() =>
            {
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, new UserListRequestEvent(), "Mario Cohl");
                _host.Host.ReportEvent(serializedCommand);
            });
        }


        #endregion

        #endregion
    }
}
