﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CAALHP.Events;
using CAALHP.SOA.ICE.ClientAdapters;
using COPDlib;
using COPDlib.Annotations;
using CustomTeleObservation;
using MarioKol.Views;

namespace MarioKol.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private DataManager _dataManager;
        private List<UserScore> _userScores;
        private ObservableCollection<UserScore> _topTen;
        private Copd _lastExcerciseCopd;
        private CopdImplementation _imp;
        private AppAdapter _adapter;
        private MainMenu _mainWindow;
        private GameWindow _gameWindow;
        private string _name;
        private int _avgHr;
        private int _minSpo;
        private int _maxSpo;
        private double _duration;
        private Oximeter _oximeter;

        #region Last exercise

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public int AvgHr
        {
            get { return _avgHr; }
            set
            {
                if (value == _avgHr) return;
                _avgHr = value;
                OnPropertyChanged();
            }
        }

        public int MinSpo
        {
            get { return _minSpo; }
            set
            {
                if (value == _minSpo) return;
                _minSpo = value;
                OnPropertyChanged();
            }
        }

        public int MaxSpo
        {
            get { return _maxSpo; }
            set
            {
                if (value == _maxSpo) return;
                _maxSpo = value;
                OnPropertyChanged();
            }
        }

        public double Duration
        {
            get { return _duration; }
            set
            {
                if (value.Equals(_duration)) return;
                _duration = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public ObservableCollection<UserScore> TopTen
        {
            get { return _topTen; }
            set { _topTen = value; }
        }

        public MainWindowViewModel(DataManager dm, MainMenu mainMenu)
        {
            _oximeter = new Oximeter();

            ConnectToCaalhp();
            _mainWindow = mainMenu;
            TopTen = new ObservableCollection<UserScore>();
            _dataManager = dm;
            _userScores = _dataManager.GetAllUserScores();
            UpdateLastExercise();
            GetTopTenScores();
            _oximeter.StartExerciseEvent += StartExercise;

            //Thread t = new Thread(_oximeter.SearchForBluetooth);
            //t.SetApartmentState(ApartmentState.STA);
            //t.Start();

            _oximeter.SearchForBluetooth();
        }

        private void StartExercise(Oximeter oxi, EventArgs e)
        {
            _mainWindow.Button_Click(null, null);
        }

        private void ConnectToCaalhp()
        {
            // connecting to Caalhp adapter
            const string endpoint = "localhost";
            try
            {
                _imp = new CopdImplementation(this);
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch (Ice.Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void GetTopTenScores()
        {

            if (_userScores.Count > 9)
            {
                for (int i = 0; i < 10; i++)
                {
                    TopTen.Add(_userScores[i]);
                }
            }
            else
            {
                foreach (UserScore score in _userScores)
                {
                    TopTen.Add(score);
                }
            }
        }

        public void Subscribe(DataManager dataManager)
        {
            dataManager.UserChangedEvent += new DataManager.EventHandler(UpdateLastExercise);
        }

        public void UpdateLastExercise()
        {
            Copd copd = _dataManager.GetLastMeasuremenForUser();
            Name = _dataManager.getUserName();
            MinSpo = copd.MinSpo;
            MaxSpo = copd.MaxSpo;
            AvgHr = copd.AvgHr;  

            TimeSpan ts = copd.EndTime.Subtract(copd.StartTime);

            Duration = ts.TotalMinutes;
        }


        #region INotifyPropertyChanged
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public void Show()
        {
            if (_mainWindow != null) _mainWindow.Show();
        }
    }
}
