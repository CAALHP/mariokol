﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using CAALHP.Events.Types;
using COPDlib;
using COPDlib.Annotations;
using CustomTeleObservation;
using MarioKol.Views;
using Exception = Ice.Exception;
using System.IO;
using Brush = System.Drawing.Brush;

namespace MarioKol.ViewModels
{
    public class GameWindowViewModel : INotifyPropertyChanged
    {
        private CopdImplementation _imp;
        DispatcherTimer _timer;
        TimeSpan _time;
        private string _fullName;
        public Oximeter Oximeter;
        private DataManager _dataManager;

        private Game _game;
        private GameWindow _window;
        private int _scoreTime;
    
        private DateTime exerciseStartTime;
        private DateTime exerciseEndTime;

        private const int saturationUpperThreshold  = 90;   // Alle værdier over anses som acceptable 
        private const int saturationLowerThreshold = 85;    // Alle værdier under kritiske

        public GameWindowViewModel(string deviceName, string comPort, int scoreTime, GameWindow window, DataManager dm)
        {
            if (window != null) _window = window;
            setupDeviceConnection(comPort, deviceName);
            //connectToCaalhp();
            startGame();
            _scoreTime = scoreTime;
            _dataManager = dm;
        }

        private void startGame()
        {
            _game = new Game();
            _game.openGame();

            TimerStart();
        }
        
        private void setupDeviceConnection(string comPort, string deviceName)
        {
            Oximeter = new Oximeter("Test10", comPort, deviceName);

            this.Subscribe(Oximeter);
        }


        public string FullName
        {
            get { return _fullName; }
            private set
            {
                if (value != null)
                {
                    if (value == _fullName) return;
                    _fullName = value;
                    OnPropertyChanged();
                }
            }
        }

        private void TimerStart()
        {
            exerciseStartTime = DateTime.Now;

            Oximeter.StartExercise();

            _window.LblDuration.Foreground = Brushes.White;

            _time = TimeSpan.FromMinutes(0);
            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                _window.LblDuration.Content = _time.ToString("c");
                UpdateUI();

                _time = _time.Add(TimeSpan.FromSeconds(1));
            }, Application.Current.Dispatcher);

            _timer.Start();
        }

        private void UpdateUI()
        {
            _window.LblHeartRate.Content = Oximeter.Puls.ToString();
            _window.LblConnectionStatus.Content = Oximeter.ConnectionStatus;


            _window.LblBloodSaturation.Content = Oximeter.Saturation.ToString();

            if (Oximeter.Saturation < saturationLowerThreshold)                                                         // Values under lower threshold
            {
                _window.LblBloodSaturation.Background = Brushes.Red;  // Red color
            }
            else if (Oximeter.Saturation > saturationLowerThreshold && Oximeter.Saturation < saturationUpperThreshold)  // Values between lower and upper threshold              
            {
                _window.LblBloodSaturation.Background = Brushes.Yellow;  // Yellow color
            }
            else
            {
                _window.LblBloodSaturation.Background = Brushes.GreenYellow; // Green color
            }
        }


        public void Subscribe(Oximeter oximeter)
        {
            oximeter.EndExerciseEvent += new Oximeter.EventHandler(endExercise);
        }

        public void SubscribeToCopdImplementation(CopdImplementation copdImplementation)
        {
            copdImplementation.Event += new CopdImplementation.EventHandler(updateUser);
        }

        private void updateUser(UserProfile userProfile, EventArgs e)
        {
            _dataManager.setUserName(userProfile.FullName);
        }

        private void endExercise(Oximeter oxi, EventArgs e)
        {
            _timer.Stop();
            Oximeter.StopExercise();

            exerciseEndTime = DateTime.Now;

            Copd copd = new Copd(Oximeter.CalculateAverageHr(), Oximeter.FindMinimumBloodSaturation(), Oximeter.FindMaximumBloodSaturation(), exerciseStartTime, exerciseEndTime);
            
            //TODO: fix user profile 
            _dataManager.WriteCopdToFile(copd);

            Oximeter.ResetMeasurements();

            _game.CloseGame();
        }


        private List<string> getUsersFromFile()
        {
            List<string> tmp = new List<string>();
            
            using (StreamReader sr = new StreamReader(@"C:\CareStoreRepo\mario-coohl\COPDprototype\Mario Coohl\resources\UserIds.txt"))
            {
                string line;
                // Read and display lines from the file until the end of  
                // the file is reached. 
                while ((line = sr.ReadLine()) != null)
                {
                    tmp.Add(line);   
                }
            }

            return tmp;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
