﻿using System.Diagnostics;
using System.Windows;
using COPDlib;
using MarioKol.ViewModels;

namespace MarioKol.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        private MainWindowViewModel _vm;
        private DataManager _dm;
        
        public MainMenu()
        {
            InitializeComponent();

           _dm = new DataManager();
           _vm = new MainWindowViewModel(_dm, this);
           DataContext = _vm;
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            var win = new GameWindow(_dm);
            win.Show();
        }

        private void Window_Activated(object sender, System.EventArgs e)
        {
            _vm.UpdateLastExercise();
        }
    }
}
