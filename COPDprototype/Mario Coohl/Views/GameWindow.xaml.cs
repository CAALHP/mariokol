﻿using System.Configuration;
using COPDlib;
using MarioKol.ViewModels;

namespace MarioKol.Views
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class GameWindow
    {
        private GameWindowViewModel _vm;

        public GameWindow(DataManager dm)
        {
            InitializeComponent();

            //Todo: Lad GameWindowViewModel oprettelse af implemenationsfilen. Dette eksponerer også User.FullName til databinding!

            // getting appSettings
            string deviceName = ConfigurationManager.AppSettings["deviceName"];
            string comPort = ConfigurationManager.AppSettings["comPort"];
            int ScoreTime = int.Parse(ConfigurationManager.AppSettings["ScoreTime"]);

            // Setting datacontext
            _vm = new GameWindowViewModel(deviceName, comPort, ScoreTime, this, dm);
            DataContext = _vm;
        }
    }
}
