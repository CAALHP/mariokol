﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace COPDlib
{
    public class Game
    {
        private Process game;
        public Game()
        {
            
        }

        public void openGame()
        {
            string EmulatorPath = ConfigurationManager.AppSettings["EmulatorPath"];
            string GamePath = ConfigurationManager.AppSettings["GamePath"];

            game = new Process();
            game.StartInfo.FileName = EmulatorPath;
            game.StartInfo.Arguments = GamePath;
            game.Start();
        }

        public void CloseGame()
        {
            game.Kill();
        }

        public void enterGame()
        {
            InputSimulator simulator = new InputSimulator();

            simulator.Keyboard.KeyPress(VirtualKeyCode.RETURN);
            Thread.Sleep(5000);

            for (int i = 0; i < 7; i++)
            {
                simulator.Keyboard.KeyPress(VirtualKeyCode.RETURN);
                Thread.Sleep(1000);    
            }

            for (int i = 0; i < 2; i++)
            {
                simulator.Keyboard.KeyPress(VirtualKeyCode.RIGHT);
                Thread.Sleep(500);
            }

            for (int i = 0; i < 2; i++)
            {
                simulator.Keyboard.KeyPress(VirtualKeyCode.RETURN);
                Thread.Sleep(500);
            }
            

        }
    }
}
