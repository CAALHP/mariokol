﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPDlib
{
    public class Measurement
    {
        public int HeartRate { get; set; }
        public int BloodSaturation { get; set; }


        public Measurement(int heartRate, int bloodSaturation)
        {
            HeartRate = heartRate;
            BloodSaturation = bloodSaturation;
        }
    }
}
