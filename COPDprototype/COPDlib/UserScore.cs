﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPDlib
{
    public class UserScore : IComparable
    {
        public string UserName { get; set; }
        public int Duration { get; set; }
        public int Rank { get; set; }


        public UserScore(string userName, int duration)
        {
            Rank = 0;
            UserName = userName;
            Duration = duration;
        }

        public int CompareTo(object obj)
        {
            UserScore userScore = (UserScore) obj;

            return this.Duration.CompareTo(userScore.Duration);
        }
    }
}
