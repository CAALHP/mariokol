﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomTeleObservation;

namespace COPDlib
{
    public class DataManager
    {
        // event stuff
        public event EventHandler UserChangedEvent;
        public EventArgs e = null;
        public delegate void EventHandler();

        private string _userName;

        public void setUserName(string name)
        {
            if (name != null)
            {
                _userName = name;
                UserChangedEvent();
            }
        }

        public string getUserName()
        {
            if (_userName != null)
            {
                return _userName;
            }
            return "Unknown";
        }

        public DataManager()
        {
            
        }
       
        public void WriteCopdToFile(Copd measurement)
        {
            string fileName = getUserName() + ".mcd";
            StreamWriter writer = new StreamWriter(fileName, true);

            if (measurement != null)
            {
                writer.WriteLine(measurement.ToString());
                writer.Close();
            }
            else
            {
                DateTime time = new DateTime();
                writer.WriteLine("0,0,0," + time.Ticks.ToString() + "," + time.Ticks.ToString());
                writer.Close();
            }
            
        }

        public List<Copd> GetDataForUser()
        {
            List<Copd> copds = new List<Copd>();

            string fileName = getUserName() + ".mcd";
           
            string[] lines = System.IO.File.ReadAllLines(fileName);


            foreach (string line in lines)
            {
                string[] values = line.Split(',');

                int avgHr =     Convert.ToInt16(values[0]);
                int minSpo =    Convert.ToInt16(values[1]);
                int maxSpo =    Convert.ToInt16(values[2]);

                DateTime start = new DateTime(Convert.ToInt64(values[3]));
                DateTime end = new DateTime(Convert.ToInt64(values[4]));

                Copd measurement = new Copd(avgHr, minSpo, maxSpo, start, end);

                copds.Add(measurement);
            }

            return copds;
        }

        public List<Copd> GetDataForUser(string name)
        {
            List<Copd> copds = new List<Copd>();

            string fileName = name + ".mcd";

            string[] lines = System.IO.File.ReadAllLines(fileName);

            foreach (string line in lines)
            {
                string[] values = line.Split(',');

                int avgHr = Convert.ToInt16(values[0]);
                int minSpo = Convert.ToInt16(values[1]);
                int maxSpo = Convert.ToInt16(values[2]);

                DateTime start = new DateTime(Convert.ToInt64(values[3]));
                DateTime end = new DateTime(Convert.ToInt64(values[4]));

                Copd measurement = new Copd(avgHr, minSpo, maxSpo, start, end);

                copds.Add(measurement);
            }

            return copds;
        }

        public List<UserScore> GetAllUserScores()
        {
            List<string> users = new List<string>();
            string[] filePaths = Directory.GetFiles(@"../", "*.mcd",
                                         SearchOption.AllDirectories);

            if (filePaths.Count() == 0)
            {
                Copd emptyMeasurement = null;

                WriteCopdToFile(emptyMeasurement);

                filePaths = Directory.GetFiles(@"../", "*.mcd",
                                         SearchOption.AllDirectories);
            }


            foreach (string filePath in filePaths)
            {
                string[] name = filePath.Split('\\');
                string[] nameWithoutExtension = name[name.Count() - 1].Split('.');

                users.Add(nameWithoutExtension[0]);
            }

             
            List<List<Copd>> allUsersMeasurements = new List<List<Copd>>();

            foreach (string user in users)
            {
                allUsersMeasurements.Add(GetDataForUser(user));
            }

            List<UserScore> allUsersScores = new List<UserScore>();

            for (int i = 0; i < allUsersMeasurements.Count; i++)
            {
                allUsersScores.Add(calculateUserScore(allUsersMeasurements[i], users[i]));
            }
            
            allUsersScores.Sort();
            allUsersScores.Reverse();

            for (int i = 0; i < allUsersScores.Count; i++)
            {
                allUsersScores[i].Rank = i + 1;
            }

            return allUsersScores;
        }

        private UserScore calculateUserScore(List<Copd> measurements, string name)
        {
            int score = 0;

            foreach (Copd measurement in measurements)
            {
                TimeSpan duration = measurement.EndTime.Subtract(measurement.StartTime);

                score += Convert.ToInt16(duration.TotalMinutes);
            }

            return new UserScore(name, score);
        }

        public Copd GetLastMeasuremenForUser()
        {
            List<Copd> copds = new List<Copd>();

            string fileName = getUserName() + ".mcd";

            string[] lines = System.IO.File.ReadAllLines(fileName);

            foreach (string line in lines)
            {
                string[] values = line.Split(',');

                int avgHr = Convert.ToInt16(values[0]);
                int minSpo = Convert.ToInt16(values[1]);
                int maxSpo = Convert.ToInt16(values[2]);

                DateTime start = new DateTime(Convert.ToInt64(values[3]));
                DateTime end = new DateTime(Convert.ToInt64(values[4]));

                Copd measurement = new Copd(avgHr, minSpo, maxSpo, start, end);

                copds.Add(measurement);
            }

            return copds.Last();
        }


    }
}
