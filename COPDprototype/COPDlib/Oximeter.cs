﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading;
using COPDlib.Annotations;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;

namespace COPDlib
{
    public class Oximeter : INotifyPropertyChanged // : IOximeter
    {
        // event stuff
        public event EventHandler EndExerciseEvent;
        public event EventHandler StartExerciseEvent;
        public EventArgs e = null;

        public delegate void EventHandler(Oximeter oximeter, EventArgs e);

        private List<Measurement> measurements = new List<Measurement>();
        private string FileName;

        private SerialPort port;
        private int iterations = 0;

        private string NONIN_DEVICE = "Nonin_Medical_Inc._680956";

        private bool Running = false;

        public Oximeter(string _fileName, string COMport, string noninDevice)
        {
            port = new SerialPort(COMport, 9600);

            NONIN_DEVICE = noninDevice;
            FileName = _fileName + ".txt";

            StartRfidAndBluetoothThreads();

            var portNames = SerialPort.GetPortNames();

            foreach (var portName in portNames)
            {
                if (portName == COMport)
                {
                    port.Open();
                }
            }
        }

        public Oximeter()
        {
            Puls = 0;
        }

        private int _testData;

        public int TestData
        {
            get { return _testData; }
            set
            {
                if (_testData != value)
                {
                    _testData = value;
                    RaisePropertyChanged(() => TestData);
                }
            }
        }

        private int _puls;

        public int Puls
        {
            get { return _puls; }
            set
            {
                if (_puls != value && value != 0)
                {
                    _puls = value;
                    RaisePropertyChanged(() => Puls);
                }

                if (value == 0)
                {
                    ConnectionStatus = "Connecting...";
                }
                else
                {
                    ConnectionStatus = "";
                }
            }
        }

        private int _saturation;

        public int Saturation
        {
            get { return _saturation; }
            set
            {
                if (_saturation != value)
                {
                    _saturation = value;
                    RaisePropertyChanged(() => Saturation);
                }
            }
        }

        private string _connectionStatus;

        public string ConnectionStatus
        {
            get { return _connectionStatus; }
            set
            {
                if (_connectionStatus != value)
                {
                    _connectionStatus = value;
                    RaisePropertyChanged(() => ConnectionStatus);
                }

            }
        }

        private bool _bluetoothReady;

        public bool BluetoothReady
        {
            get { return _bluetoothReady; }
            set { _bluetoothReady = value; }
        }

        private void StartRfidAndBluetoothThreads()
        {
            var bw2 = new BackgroundWorker();
            bw2.DoWork += SetupNoninAndStartThread;
            bw2.RunWorkerAsync();
        }

        private BluetoothClient _bluetooth; // = new BluetoothClient();
        private int counter = 0;

        private void SetupNoninAndStartThread(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            _bluetooth = new BluetoothClient();
            var t = new Thread(GetData);
            t.Name = "Nonin Thread";
            var gadgeteerDevice = _bluetooth.DiscoverDevices().FirstOrDefault(x => x.DeviceName == NONIN_DEVICE);
            bool run = false;

            ConnectionStatus = "Connecting...";

            while (!run)
            {
                try
                {
                    if (gadgeteerDevice != null)
                    {
                        _bluetooth.Connect(gadgeteerDevice.DeviceAddress, BluetoothService.SerialPort);
                        if (_bluetooth.Connected)
                        {
                            run = true;
                            //setting format (sending measurements every second)
                            var startByte = new Byte[6];
                            startByte[0] = 2;
                            startByte[1] = 112;
                            startByte[2] = 2;
                            startByte[3] = 2;
                            startByte[4] = 08;
                            startByte[5] = 3;
                            _bluetooth.Client.Send(startByte);

                            t.Start();
                        }
                    }
                }
                catch (System.Exception e)
                {
                    Thread.Sleep(1000);
                }
            }
        }

        private void GetData()
        {
            int lastPulsMeasurement = 0;
            int pulsCounter = 0;

            

            while (true)
            {
                int sizeOfReading = _bluetooth.Available;
                BluetoothReady = _bluetooth.Available > 0; // Sets the Bluetooth ready
                var buffer = new byte[1024];
                _bluetooth.Client.Receive(buffer);


                counter++;

                if (buffer[0] == 160)
                {
                    Debug.WriteLine(counter + ": Not Sending");

                    // inform user that com is trying to connect
                }
                else if (buffer[0] == 0)
                {
                    _bluetooth.Dispose();

                    // Send event
                    EndExerciseEvent(this, e);
                    return;

                }
                else if (buffer[0] == 128)
                {
                    Debug.WriteLine(counter + ": Data recieved");

                    int tempSat = buffer[2];
                    int puls = buffer[1];

                    if (lastPulsMeasurement == puls)
                    {
                        pulsCounter++;
                    }
                    else
                    {
                        pulsCounter = 0;
                    }

                    lastPulsMeasurement = puls;

                    if (tempSat <= 100 && tempSat > 30 && puls > 0 && puls < 220 && pulsCounter < 4)
                    {
                        if (Running)
                        {
                            Measurement measurement = new Measurement(puls, tempSat);

                            measurements.Add(measurement);
                        }
                        
                        Puls = puls;
                        Saturation = tempSat;
                        
                    }
                    else
                    {
                        Puls = 0;
                    }


                    iterations++;

                    if (iterations > 10)
                    {
                        if (measurements.Count > 0)
                        {
                            if (port.IsOpen)
                            {
                                port.Write(measurements.Last().HeartRate.ToString() + '\n');
                                iterations = 0;
                            }
                        }
                    }

                }
                else
                {
                    Puls = 0;
                    Debug.WriteLine(counter + ": unknown status: " + buffer[0]);
                }
                Thread.Sleep(1000);
            }

        }


        public int CalculateAverageHr()
        {
            int sum = 0;
            for (int i = 1; i < measurements.Count; i++)
            {
                sum += measurements.ElementAt(i).HeartRate;
            }

            try
            {
                return sum/measurements.Count;
            }
            catch
            {
                return 0;
            }
        }

        public int FindMinimumBloodSaturation()
        {
            int spo = 100;
            for (int i = 0; i < measurements.Count; i++)
            {
                if (measurements.ElementAt(i).BloodSaturation < spo)
                {
                    spo = measurements.ElementAt(i).BloodSaturation;
                }
            }

            try
            {
                return spo;
            }
            catch
            {
                return 0;
            }
        }

        public int FindMaximumBloodSaturation()
        {
            int spo = 0;
            for (int i = 0; i < measurements.Count; i++)
            {
                if (measurements.ElementAt(i).BloodSaturation > spo)
                {
                    spo = measurements.ElementAt(i).BloodSaturation;
                }
            }

            try
            {
                return spo;
            }
            catch
            {
                return 0;
            }
        }

        public void ResetMeasurements()
        {
            measurements.Clear();
        }

        public void StartExercise()
        {
            Running = true;
        }

        public void StopExercise()
        {
            Running = false;
        }

        public void SearchForBluetooth()
        {
            BluetoothClient _bluetooth = new BluetoothClient();
            const string NONIN_DEVICE = "Nonin_Medical_Inc._680956";

            var gadgeteerDevice = _bluetooth.DiscoverDevices().FirstOrDefault(x => x.DeviceName == NONIN_DEVICE);
            bool run = false;


            while (!run)
            {
                try
                {
                    if (gadgeteerDevice != null)
                    {
                        _bluetooth.Connect(gadgeteerDevice.DeviceAddress,
                            InTheHand.Net.Bluetooth.BluetoothService.SerialPort);
                        if (_bluetooth.Connected)
                        {
                            run = true;
                            StartExerciseEvent(this, e);

                            _bluetooth.Dispose();
                            _bluetooth.Close();

                            return;
                        }
                    }
                }
                catch (System.Exception e)
                {
                    Thread.Sleep(1000);
                }
            }
        }

    public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RaisePropertyChanged<T>(Expression<Func<T>> action)
        {
            var propertyName = GetPropertyName(action);
            RaisePropertyChanged(propertyName);
        }

        private static string GetPropertyName<T>(Expression<Func<T>> action)
        {
            var expression = (MemberExpression)action.Body;
            var propertyName = expression.Member.Name;
            return propertyName;
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
