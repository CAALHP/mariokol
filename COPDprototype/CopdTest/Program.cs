﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COPDlib;
using CustomTeleObservation;

namespace CopdTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime start = new DateTime(2014, 06, 16, 10, 00, 00);
            DateTime end = new DateTime(2014, 06, 16, 10, 30, 00);

            Copd measurement = new Copd(104, 90, 100, start, end);

            DataManager dm = new DataManager();

            dm.WriteCopdToFile(measurement);

            List<Copd> list = dm.GetDataForUser();

            dm.GetAllUserScores();

            foreach (Copd copd in list)
            {
                Console.WriteLine(copd.ToString());
            }

            while (true)
            {
                
            }
        }
    }
}
