﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using CAALHP.SOA.ICE.ClientAdapters;
using COPDlib;
using CustomTeleObservation;


namespace Plugins.COPDprototype
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer _timer;
        TimeSpan _time;

        Game game = new Game();

        private Oximeter oximeter;
        private N4CHelper n4CHelper;
        private CopdImplementation _imp;
        private AppAdapter _adapter;

        private DateTime exerciseStartTime;
        private DateTime exerciseEndTime;

        private bool state = false;


        public MainWindow()
        {
            InitializeComponent();

            string deviceName = ConfigurationManager.AppSettings["deviceName"];
            string comPort = ConfigurationManager.AppSettings["comPort"];

            const string endpoint = "localhost";
            try
            {
                _imp = new CopdImplementation();
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            oximeter = new Oximeter("Test10", comPort, deviceName );
            n4CHelper = new N4CHelper("http://mumevm2012.cloudapp.net/observation"); // http://mumevm2012.cloudapp.net/observation  ,  http://localhost:8082/observation
            DataContext = oximeter;
            
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void BtnStartStop_Click(object sender, RoutedEventArgs e)
        {
            state = !state;

            if (state)
            {
                BtnStartStop.Content = "Stop Exercise";
                exerciseStartTime = DateTime.Now;

                oximeter.StartExercise();

                LblDuration.Foreground = Brushes.Red;

                _time = TimeSpan.FromMinutes(0);
                _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
                    {
                        LblDuration.Content = _time.ToString("c");

                        _time = _time.Add(TimeSpan.FromSeconds(1));
                    }, Application.Current.Dispatcher);

                _timer.Start();

            }
            else
            {
                _timer.Stop();

                oximeter.StopExercise();

                BtnStartStop.Content = "Start Exercise";
                exerciseEndTime = DateTime.Now;

                Copd copd = new Copd(oximeter.CalculateAverageHr(), oximeter.FindMinimumBloodSaturation(), oximeter.FindMaximumBloodSaturation(), exerciseStartTime, exerciseEndTime);

                n4CHelper.UploadCopdObservation("251248-4916", copd);

                oximeter.ResetMeasurements();

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BtnGame.Visibility = Visibility.Hidden;

            Game myGame = new Game();

            myGame.openGame();
        }
    }
}
