﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace Plugins.COPDprototype
{
    public class CopdImplementation : CAALHP.Contracts.IAppCAALHPContract
    {
        // Test commit
        private IAppHostCAALHPContract _host { get; set; }
        private Thread _thread;
        private int _processId;
        private MainWindow _currentWindow;
        private volatile Application _app;
        private const string AppName = "Mario Cohl";
        private MainWindow _main;

        public CopdImplementation()
        {
            _main = new MainWindow();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                Show();
            }
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            DoTheMessagePump();
            return true;
        }

        public void ShutDown()
        {
            // _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            // Subscribe to ShowAppEvents
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
        }

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        public void Show()
        {
            _main.Show();
        }

        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoTheMessagePump()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

    }
}
